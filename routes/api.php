<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'auth',
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});

Route::get('degree', function() {   return new \App\Http\Resources\DegreeCollection(\App\Degree::paginate(10)); });
Route::get('degree/{id}', 'DegreeController@show');
Route::post('degree', 'DegreeController@store');
Route::put('degree/{id}', 'DegreeController@update');
Route::delete('degree/{id}', 'DegreeController@destroy');

Route::get('language', function() { return new \App\Http\Resources\LanguageCollection(\App\Language::paginate(10));  })->name('get.language');
Route::get('language/{id}', 'LanguageController@show');
Route::post('language', 'LanguageController@store');
Route::put('language/{id}', 'LanguageController@update');
Route::delete('language/{id}', 'LanguageController@destroy');

Route::get('country', function() {  return new \App\Http\Resources\CountryCollection(\App\Country::paginate(10));    });
Route::get('country/{id}', 'CountryController@show');
Route::post('country', 'CountryController@store');
Route::put('country/{id}', 'CountryController@update');
Route::delete('country/{id}', 'CountryController@destroy');

Route::get('company', function() {  return new \App\Http\Resources\CompanyCollection(\App\Company::paginate(10));    });
Route::get('company/{id}', 'CompanyController@show');
Route::post('company', 'CompanyController@store');
Route::put('company/{id}', 'CompanyController@update');
Route::delete('company/{id}', 'CompanyController@destroy');