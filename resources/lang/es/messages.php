<?php

return [
    /*
    Endpoints responses
    */
    'database' => [
        'fail' => 'El recurso no pudo ser almacenado',
    ],
    'degree' => [
        'not_found' => 'Grado no encontrado',
        'id_not_found' => 'Grado con ID :id no encontrado',
        'found' => 'Grado encontrado',
        'new' => 'Grado creado',
        'update' => 'Grado actualizado',
        'delete' => 'Grado borrado'
    ],
    'language' => [
        'not_found' => 'Idioma no encontrado',
        'id_not_found' => 'Idioma con ID :id no encontrado',
        'found' => 'Idioma encontrado',
        'new' => 'Idioma creado',
        'update' => 'Idioma actualizado',
        'delete' => 'Idioma borrado'
    ],
    'country' => [
        'not_found' => 'País no encontrado',
        'id_not_found' => 'País con ID :id no encontrado',
        'found' => 'País encontrado',
        'new' => 'País creado',
        'update' => 'País actualizado',
        'delete' => 'País borrado'
    ],
];