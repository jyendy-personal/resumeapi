<?php

return [
    /*
    Endpoints responses
    */
    'database' => [
        'fail' => 'Resource could not be stored',
    ],
    'degree' => [
        'not_found' => 'Degrees not found',
        'id_not_found' => 'Degree with ID :id not found',
        'found' => 'Retrieved degree',
        'new' => 'Created degree',
        'update' => 'Updated degree',
        'delete' => 'Deleted degree'
    ],
    'language' => [
        'not_found' => 'languages not found',
        'id_not_found' => 'language with ID :id not found',
        'found' => 'Retrieved language',
        'new' => 'Created language',
        'update' => 'Updated language',
        'delete' => 'Deleted language'
    ],
    'country' => [
        'not_found' => 'country not found',
        'id_not_found' => 'country with ID :id not found',
        'found' => 'Retrieved country',
        'new' => 'Created country',
        'update' => 'Updated country',
        'delete' => 'Deleted country'
    ],
];