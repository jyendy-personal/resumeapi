<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LanguagesRoutesTest extends TestCase
{
    protected $endpoint;
    protected $model;

    public function setUp() : void {
        parent::setUp();

        $this->endpoint = '/api/language';
        $this->model = new \App\Language();
    }

    public function testExistGetRoute() {
        $response = $this->json('GET', $this->endpoint)
            ->assertStatus(200);
    }
    
    public function testExistPostPayloadRoute() {
        $response = $this->json('POST', $this->endpoint, [ 'name' => 'UnitTest'])
            ->assertStatus(201);
    }

    public function testExistGetParamsRoute() {
        $obj = $this->model::where(['name' => 'UnitTest'])->first();

        $response = $this->json('GET', $this->endpoint.'/'.$obj->getKey())
            ->assertStatus(200);
    }

    public function testExistPutPayloadRoute() {
        $obj = $this->model::where(['name' => 'UnitTest'])->first();

        $response = $this->json('PUT', $this->endpoint.'/'.$obj->getKey(), [ "name" => "UnitTest"])
            ->assertStatus(204);
    }

    public function testExistDeletePayloadRoute() {
        $obj = $this->model::where(['name' => 'UnitTest'])->first();

        $response = $this->json('DELETE', $this->endpoint.'/'.$obj->getKey())
            ->assertStatus(204);
    }
}
