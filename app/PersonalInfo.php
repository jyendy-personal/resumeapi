<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonalInfo extends Model
{
    protected $table = 'personal_info';

    protected $primaryKey = 'id_personal_info';

    protected $fillable = [
        'full_name',
        'bio',
        'birthday_date',
        'born_country_id',
        'phone',
        'celular_phone',
        'website',
        'additional_info',
        'id_user',
    ];
}
