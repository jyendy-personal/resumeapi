<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $table = 'schools';

    protected $primaryKey = 'id_school';

    protected $fillable = [
        'name',
        'id_country',
        'location',
    ];
}
