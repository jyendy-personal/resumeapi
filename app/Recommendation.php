<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recommendation extends Model
{
    protected $table = 'recommendations';

    protected $primaryKey = 'id_recommendation';

    protected $fillable = [
        'person_name',
        'role',
        'comment',
        'id_resume',
    ];
}
