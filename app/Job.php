<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $table = 'jobs';

    protected $primaryKey = 'id_job';

    protected $fillable = [
        'start_date',
        'end_date',
        'is_actual',
        'id_company',
        'role',
        'location_job',
        'visible',
        'description',
        'achievements',
        'contact_name',
        'contact_phone',
        'contact_email',
        'id_resume',
    ];
}
