<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';

    protected $primaryKey = 'id_country';

    protected $fillable = [
        'name',
        'code_iso2',
        'code_iso3',
    ];
}
