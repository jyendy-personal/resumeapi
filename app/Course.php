<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'courses';

    protected $primaryKey = 'id_course';

    protected $fillable = [
        'name',
        'description',
        'issue_date',
        'expiration_date',
        'issuing_organization',
        'credential_id',
        'credential_url',
        'id_resume',
        'is_certification',
    ];
}
