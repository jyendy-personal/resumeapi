<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
 * @OA\Info(title="RESUME API", version="2.0")
 * 
 */

 //@ O A\Server(url="http://localhost/cvapi/public", description="OpenApi specifications for Resume API v1")

/**
 * @OA\SecurityScheme(
 *     description="Use a username and password to obtain a token with /api/auth/login endpoint",
 *     name="Password Based",
 *     scheme="bearer",
 *     securityScheme="bearerAuth",
 *     type="http",

 * )
 */

class AuthController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get a JWT via given credentials.
     * 
     * This endpoint is used to authenticate API user in PagoFácil backend
     *
     * @return \Illuminate\Http\JsonResponse
     * 
     * @OA\Post(
     *     path="/api/auth/login",
     *     summary="LOGIN",
     *     tags={"Authentication - access"},
     *   @OA\RequestBody(
     *       required=false,
     *       @OA\MediaType(
     *           mediaType="application/json",
     *           @OA\Schema(
     *               type="object",
     *               @OA\Property(
     *                   property="email",
     *                   description="User provided",
     *                   type="string",
     *                   minLength=6,
     *                   maxLength=40,
     *                   example="thomas.anderson@metacortex.com"
     *               ),
     *               @OA\Property(
     *                   property="password",
     *                   description="Password provided",
     *                   type="string",
     *                   minLength=8,
     *                   maxLength=255,
     *                   example="theone"
     *               ),
     *               required={"user", "password"}
     *           )
     *       )
     *   ),
     *     @OA\Response(
     *         response=200,
     *         description="Autenticación exitosa. Se ha generado un access_token."
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="Ha ocurrido un error."
     *     )
     * )
     * 
     */
    public function login()
    {
        $credentials = request(['email', 'password']);
        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $this->respondWithToken($token);
    }
    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }
    public function payload()
    {
        return response()->json(auth()->payload());
    }
    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }
    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }
    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user(),
        ]);
    }
}
