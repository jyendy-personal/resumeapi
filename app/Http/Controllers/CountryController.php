<?php

namespace App\Http\Controllers;

use App\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;

class CountryController extends BaseController
{
    protected $model;

    public function __construct()
    {
        $this->model = new Country();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $newObj = $this->model::all();
        return $this->sendResponse($newObj, __('messages.country.found'), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3|max:40',
            'code_iso2' => 'required|string|min:2|max:2',
            'code_iso3' => 'required|string|min:3|max:4',
        ]);
        
        if ($validator->fails()) {
            return $this->sendError(__('validation.fail'), $validator->errors(), 400);
        }

        $newObj = $this->model::create($request->all());

        return $this->sendResponse($newObj, __('messages.country.new'), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!preg_match('/^[0-9]+$/', $id)) {
            return $this->sendError(__('validation.fail').'-'.'ID not allowed', [], 400);
        }

        $newObj = $this->model::find($id);

        if(!$newObj) {
            return $this->sendError(__('messages.country.not_found'), [], 404);
        }

        return $this->sendResponse($newObj, __('messages.country.found'), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3|max:400',
        ]);
        
        if ($validator->fails()) {
            return $this->sendError(__('validation.fail'), $validator->errors(), 400);
        }

        if(!preg_match('/^[0-9]+$/', $id)) {
            return $this->sendError(__('validation.fail').'-'.'ID not allowed', [], 400);
        }

        $newObj = $this->model::find($id);

        if(!$newObj) {
            return $this->sendError(__('messages.country.not_found'), $request->all(), 404);
        }

        try {
            $newObj->fill($request->all());
            $newObj->save();
        } catch (QueryException $e) {
            Log::error(__('messages.databse.fail'), ['id' => $id, 'request' => $request->all(), 'exception' => $e->getMessage()]);
            return $this->sendError(__('validation.fail'), __('messages.database.fail'), 400);
        }
        
        return $this->sendResponse($newObj, __('messages.country.update'), 204);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!preg_match('/^[0-9]+$/', $id)) {
            return $this->sendError(__('validation.fail').'-'.'ID not allowed', [], 400);
        }

        $newObj = $this->model::find($id);

        if(!$newObj) {
            return $this->sendError(__('messages.country.not_found'), [], 404);
        }

        $newObj->delete();

        return $this->sendResponse($newObj, __('messages.country.delete'), 204);
    }
}
