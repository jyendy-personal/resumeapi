<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resume extends Model
{
    protected $table = 'resumes';

    protected $primaryKey = 'id_resume';

    protected $fillable = [
        'name',
        'active',
    ];
}
