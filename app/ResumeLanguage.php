<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResumeLanguage extends Model
{
    protected $table = 'resume_language';

    protected $primaryKey = 'id_resume_language';

    protected $fillable = [
        'id_resume',
        'id_language',
        'proficiency',
    ];
}
