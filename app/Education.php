<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    protected $table = 'educations';

    protected $primaryKey = 'id_education';

    protected $fillable = [
        'description',
        'field_study',
        'id_school',
        'id_degree',
        'start_year',
        'end_year',
        'id_resume',
    ];
}
