<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserResume extends Model
{
    protected $table = 'user_resume';

    protected $primaryKey = 'id_user_resume';

    protected $fillable = [
        'id_user',
        'id_resume',
        'active',
    ];
}
