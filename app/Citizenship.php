<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Citizenship extends Model
{
    protected $table = 'citizenships';

    protected $primaryKey = 'id_citizenships';

    protected $fillable = [
        'id_user',
        'id_country',
    ];
}
