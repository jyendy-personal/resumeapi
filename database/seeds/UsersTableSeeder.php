<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        User::create([
            'email' => 'jyendy@gmail.com',
            'hash_email' => hash('sha1', 'jyendy@gmail.com'),
            'password' => Hash::make('5211257964'),
            'name' => 'Juan',
            'last_name' => 'Yendy'
        ]);

        User::create([
            'email' => 'thomas.anderson@metacortex.com',
            'hash_email' => hash('sha1', 'thomas.anderson@metacortex.com'),
            'password' => Hash::make('theone'),
            'name' => 'Thomas',
            'last_name' => 'Anderson'
        ]);
    }
}
