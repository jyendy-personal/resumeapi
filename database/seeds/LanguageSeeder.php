<?php

use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->delete();

        $languages = [
            ['name' => 'English', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Spanish', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Portuguese', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Chinese', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Hindi', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Russian', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Japanese', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Arabic', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Bengali', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Punjabi', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'French', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'German', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Italian', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Polish', 'created_at' => date('Y-m-d H:i:s')],
        ];

        DB::table('languages')->insert($languages);
    }
}
