<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id('id_company');
            $table->string('name')->nullable(false);
            $table->text('description')->nullable();
            $table->string('location', 140)->nullable();
            $table->string('website', 150)->nullable();
            $table->unsignedBigInteger('id_country')->nullable(false);
            $table->timestamps();

            $table->foreign('id_country')->references('id_country')->on('countries')->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
