<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitizenshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citizenships', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id('id_citizenship');
            $table->unsignedBigInteger('id_user')->nullable(false);
            $table->unsignedBigInteger('id_country')->nullable(false);
            $table->timestamps();

            $table->foreign('id_user')->references('id')->on('users')->nullable(false);
            $table->foreign('id_country')->references('id_country')->on('countries')->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('citizenships');
    }
}
