<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id('id_course');
            $table->string('name', 140)->nullable(false);
            $table->text('description')->nullable(true);
            $table->dateTime('issue_date');
            $table->dateTime('expiration_date');
            $table->string('credential_id', 60)->nullable(true);
            $table->string('credential_url', 150)->nullable(true);
            $table->unsignedBigInteger('id_resume')->nullable(false);
            $table->boolean('is_certification')->default(false);
            $table->timestamps();

            $table->foreign('id_resume')->references('id_resume')->on('resumes')->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
