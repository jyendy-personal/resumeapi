<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id('id_school');
            $table->string('name', 80)->nullable(false);
            $table->string('location', 140)->nullable();
            $table->unsignedBigInteger('id_country')->nullable(false);
            $table->timestamps();

            $table->foreign('id_country')->references('id_country')->on('countries')->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
