<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEducationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('educations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id('id_education');
            $table->text('description')->nullable();
            $table->string('field_study')->nullable();
            $table->unsignedBigInteger('id_school')->nullable(false);
            $table->unsignedBigInteger('id_degree')->nullable(false);
            $table->unsignedBigInteger('id_resume')->nullable(false);
            $table->integer('start_year');
            $table->integer('end_year');
            $table->timestamps();

            $table->foreign('id_school')->references('id_school')->on('schools')->nullable(false);
            $table->foreign('id_degree')->references('id_degree')->on('degrees')->nullable(false);
            $table->foreign('id_resume')->references('id_resume')->on('resumes')->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('educations');
    }
}
