<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id('id_job');
            $table->dateTime('start_date')->nullable(false);
            $table->dateTime('end_date')->nullable(true);
            $table->boolean('is_actual')->default(false);
            $table->unsignedBigInteger('id_company')->nullable(false);
            $table->string('role', 100)->nullable(false);
            $table->string('location_job', 100)->nullable(true);
            $table->boolean('visible')->default(true);
            $table->text('description')->nullable();
            $table->text('achievements')->nullable();
            $table->string('contact_name', 50)->nullable(true);
            $table->string('contact_phone', 16)->nullable(false);
            $table->string('contact_email', 150)->nullable(false);
            $table->unsignedBigInteger('id_resume')->nullable(false);
            $table->timestamps();

            $table->foreign('id_resume')->references('id_resume')->on('resumes')->nullable(false);
            $table->foreign('id_company')->references('id_company')->on('companies')->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
