<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResumeLanguageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resume_language', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id('id_resume_language');
            $table->unsignedBigInteger('id_resume')->nullable(false);
            $table->unsignedBigInteger('id_language')->nullable(false);
            $table->timestamps();

            $table->foreign('id_resume')->references('id_resume')->on('resumes')->nullable(false);
            $table->foreign('id_language')->references('id_language')->on('languages')->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resume_language');
    }
}
