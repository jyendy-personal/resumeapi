<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_info', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id('id_personal_info');
            $table->string('full_name', 60)->nullable(false);
            $table->text('bio')->nullable(true);
            $table->timestamp('birthday_date')->nullable(true);
            $table->unsignedBigInteger('born_country_id')->nullable(true);
            $table->string('phone', 16)->nullable(true);
            $table->string('celular_phone', 16)->nullable(false);
            $table->string('website', 100)->nullable(true);
            $table->text('additional_info')->nullable(true);
            $table->unsignedBigInteger('id_user')->nullable(true);
            $table->timestamps();
        });

        Schema::table('personal_info', function (Blueprint $table) {
            $table->foreign('id_user')->references('id')->on('users');
            $table->foreign('born_country_id')->references('id_country')->on('countries')->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_info');
    }
}
