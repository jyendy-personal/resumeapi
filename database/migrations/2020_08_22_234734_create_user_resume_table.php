<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserResumeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_resume', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id('id_user_resume');
            $table->unsignedBigInteger('id_user')->nullable(false);
            $table->unsignedBigInteger('id_resume')->nullable(false);
            $table->boolean('active')->default(true);
            $table->timestamps();

            $table->foreign('id_user')->references('id')->on('users')->nullable(false);
            $table->foreign('id_resume')->references('id_resume')->on('resumes')->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_resume');
    }
}
