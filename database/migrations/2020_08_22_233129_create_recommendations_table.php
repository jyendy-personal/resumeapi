<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecommendationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recommendations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id('id_recommendation');
            $table->string('person_name', 50)->nullable(false);
            $table->string('role', 40)->nullable(false);
            $table->text('comment')->nullable(false);
            $table->unsignedBigInteger('id_resume');
            $table->timestamps();

            $table->foreign('id_resume')->references('id_resume')->on('resumes')->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recommendations');
    }
}
